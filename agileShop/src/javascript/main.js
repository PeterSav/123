import fetchDevices from "./fetch.js"

const URL = `https://cors-anywhere.herokuapp.com/https://gitlab.com/IsminiD/devices-json/raw/master/devices.json`;
(async () => {
    fetchDevices();
})()
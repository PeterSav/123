// selectors gia to html manipulation
const DOMSelectors = {
    container: document.getElementById('mainContainer')
}

// using the fetched data creates smartphones array
export const getProduct = (products) => {
    return products.filter(product => product.type === 'smartphone');
}
// using the fetched data creates tablets array
export const getTablets = (products) => {
    return products.filter(product => product.type === 'tablet');
}
// using the fetched data creates non smartphones/tablets array
export const getAccessories = (products) => {
    return products.filter(product => product.type !== 'smartphone' && product.type !== 'tablet');
}

// synarthsh poy gemizei to headline (length) kai onoma twn products section
export const fillHeadLine = (productsArray, productsType) => {
    DOMSelectors.container.insertAdjacentHTML('beforeend', `<div class="section_of_products">
    <!-- left side of the headline -->
    <div>
        <h3 class="section_of_products__type">
            ${productsType}
            <a href="#" class="section_of_products__quantity">
                (${productsArray.length})
            </a>
        </h3>
    </div>
    <!-- right side of the headline -->
    <aside>
        <a href="">
            <i class=" material-icons magenta">chevron_left</i>
        </a>
        <a href="">
            <i class="material-icons magenta">chevron_right</i>
        </a>
        <a href="">
            <i class="fas fa-tasks magenta"></i>
        </a>
    </aside>
</div>`)
}

// synarthsh poy gemizei osa products zhth8oun to section pou dhmiourgei h fillHeadLine 
export const fillProducts = (productsArray, numberOfProducts) => {
    DOMSelectors.container.insertAdjacentHTML('beforeend', `<div class="container"</div>`)
    productsArray.forEach((product, index) => {
        if (index < numberOfProducts) {
            DOMSelectors.container.lastChild.insertAdjacentHTML('beforeend', `<article class="product">
            <figure class="product__photo">
                <button class="product__favorite_button">
                    <i class="material-icons" class="product__favorite_icon">favorite_border</i>
                </button>
                <div class="product__photo_box">
                    <h3 class="product__title">
                        ${product.name}
                    </h3>
                    <p class="product__subtitle">
                        ${product.description}
                    </p>
                    <a href="" class="product__discover">
                        Discover More
                    </a>
                    <div class="product__price">
                        $${product.price}
                    </div>
                    <div class="product__ex_price">
                        <span class="product__ex_price_line"></span>
                        $1000
                    </div>
                </div>
                <!-- fwtografia tou product -->
                <a href="">
                    <img src="${product.image}" alt="${product.name}" class="product__image {
                        height: 304px;
                        width: 344px;
                    }">
                </a>
            </figure>
        </article>`)
        }
    })
}


// section construct poy kalei tis synarthseis poy to xtizoun
export const constructSection = (productsArray, productsType, numberOfProducts) => {
    fillHeadLine(productsArray, productsType);
    fillProducts(productsArray, numberOfProducts);
}
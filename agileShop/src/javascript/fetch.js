import {
    getProduct,
    getTablets,
    getAccessories,
    constructSection,
} from './filterFunctions.js';


async function fetchDevices() {
    const URL = `https://cors-anywhere.herokuapp.com/https://gitlab.com/IsminiD/devices-json/raw/master/devices.json`;
    const fetchResult = fetch(URL)
    const response = await fetchResult;
    const jsonData = await response.json();
    let resData = jsonData.devices;
    resData.sort((a, b) => b.year - a.year);
    let smartphones = getProduct(resData);
    let tablets = getTablets(resData);
    let accessories = getAccessories(resData);
    constructSection(smartphones, "SmartPhones", 3);
    constructSection(tablets, "Tablets", 3);
    constructSection(accessories, "Accessories", 3);
}

export default fetchDevices
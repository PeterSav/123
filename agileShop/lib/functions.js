"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.asideButtonsShow = exports.descriptionsSet = exports.productsSet = exports.pricesSet = exports.imagesSet = exports.getAccessories = exports.getTablets = exports.getSmartphones = void 0;

var getSmartphones = function getSmartphones(myDevices) {
  return myDevices.filter(function (device) {
    return device.type === 'smartphone';
  });
};

exports.getSmartphones = getSmartphones;

var getTablets = function getTablets(myDevices) {
  return myDevices.filter(function (device) {
    return device.type === 'tablet';
  });
};

exports.getTablets = getTablets;

var getAccessories = function getAccessories(myDevices) {
  return myDevices.filter(function (device) {
    return device.type !== 'smartphone' && device.type !== 'tablet';
  });
};

exports.getAccessories = getAccessories;

var imagesSet = function imagesSet(smartphones, tablets, accessories) {
  images.forEach(function (image, index) {
    if (index < 3) {
      image.setAttribute("src", "".concat(smartphones[index].image));
    } else if (index < 6) {
      image.setAttribute("src", "".concat(tablets[index - 3].image));
    } else {
      image.setAttribute("src", "".concat(accessories[index - 6].image));
    }
  });
};

exports.imagesSet = imagesSet;

var pricesSet = function pricesSet(smartphones, tablets, accessories) {
  prices.forEach(function (price, index) {
    if (index < 3) {
      price.innerHTML = "$".concat(smartphones[index].price);
    } else if (index < 6) {
      price.innerHTML = "$".concat(tablets[index - 3].price);
    } else {
      price.innerHTML = "$".concat(accessories[index - 6].price);
    }
  });
};

exports.pricesSet = pricesSet;

var productsSet = function productsSet(smartphones, tablets, accessories) {
  products.forEach(function (product, index) {
    if (index < 3) {
      product.innerHTML = "".concat(smartphones[index].name);
    } else if (index < 6) {
      product.innerHTML = "".concat(tablets[index - 3].name);
    } else {
      product.innerHTML = "".concat(accessories[index - 6].name);
    }
  });
};

exports.productsSet = productsSet;

var descriptionsSet = function descriptionsSet(smartphones, tablets, accessories) {
  descriptions.forEach(function (description, index) {
    if (index < 3) {
      description.innerHTML = "".concat(smartphones[index].tech.display.Resolution);
    } else if (index < 6) {
      description.innerHTML = "".concat(tablets[index - 3].tech.display.Resolution);
    } else {
      description.innerHTML = "".concat(accessories[index - 6].tech.display.Resolution);
    }
  });
};

exports.descriptionsSet = descriptionsSet;

var asideButtonsShow = function asideButtonsShow(smartphones, tablets, accessories) {
  asideButtons.forEach(function (button, index) {
    if (index < 3) {
      if (smartphones.length < 3) {
        button.style.visibility = "hidden";
      }
    } else if (index < 6) {
      if (tablets.length < 3) {
        button.style.visibility = "hidden";
      }
    } else {
      if (accessories.length < 3) {
        button.style.visibility = "hidden";
      }
    }
  });
};

exports.asideButtonsShow = asideButtonsShow;
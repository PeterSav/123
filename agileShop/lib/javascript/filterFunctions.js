"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.constructSection = exports.fillProducts = exports.fillHeadLine = exports.getAccessories = exports.getTablets = exports.getProduct = void 0;
// selectors gia to html manipulation
var DOMSelectors = {
  container: document.getElementById('mainContainer') // using the fetched data creates smartphones array

};

var getProduct = function getProduct(products) {
  return products.filter(function (product) {
    return product.type === 'smartphone';
  });
}; // using the fetched data creates tablets array


exports.getProduct = getProduct;

var getTablets = function getTablets(products) {
  return products.filter(function (product) {
    return product.type === 'tablet';
  });
}; // using the fetched data creates non smartphones/tablets array


exports.getTablets = getTablets;

var getAccessories = function getAccessories(products) {
  return products.filter(function (product) {
    return product.type !== 'smartphone' && product.type !== 'tablet';
  });
}; // synarthsh poy gemizei to headline (length) kai onoma twn products section


exports.getAccessories = getAccessories;

var fillHeadLine = function fillHeadLine(productsArray, productsType) {
  DOMSelectors.container.insertAdjacentHTML('beforeend', "<div class=\"section_of_products\">\n    <!-- left side of the headline -->\n    <div>\n        <h3 class=\"section_of_products__type\">\n            ".concat(productsType, "\n            <a href=\"#\" class=\"section_of_products__quantity\">\n                (").concat(productsArray.length, ")\n            </a>\n        </h3>\n    </div>\n    <!-- right side of the headline -->\n    <aside>\n        <a href=\"\">\n            <i class=\" material-icons magenta\">chevron_left</i>\n        </a>\n        <a href=\"\">\n            <i class=\"material-icons magenta\">chevron_right</i>\n        </a>\n        <a href=\"\">\n            <i class=\"fas fa-tasks magenta\"></i>\n        </a>\n    </aside>\n</div>"));
}; // synarthsh poy gemizei osa products zhth8oun to section pou dhmiourgei h fillHeadLine 


exports.fillHeadLine = fillHeadLine;

var fillProducts = function fillProducts(productsArray, numberOfProducts) {
  DOMSelectors.container.insertAdjacentHTML('beforeend', "<div class=\"container\"</div>");
  productsArray.forEach(function (product, index) {
    if (index < numberOfProducts) {
      DOMSelectors.container.lastChild.insertAdjacentHTML('beforeend', "<article class=\"product\">\n            <figure class=\"product__photo\">\n                <button class=\"product__favorite_button\">\n                    <i class=\"material-icons\" class=\"product__favorite_icon\">favorite_border</i>\n                </button>\n                <div class=\"product__photo_box\">\n                    <h3 class=\"product__title\">\n                        ".concat(product.name, "\n                    </h3>\n                    <p class=\"product__subtitle\">\n                        ").concat(product.description, "\n                    </p>\n                    <a href=\"\" class=\"product__discover\">\n                        Discover More\n                    </a>\n                    <div class=\"product__price\">\n                        $").concat(product.price, "\n                    </div>\n                    <div class=\"product__ex_price\">\n                        <span class=\"product__ex_price_line\"></span>\n                        $1000\n                    </div>\n                </div>\n                <!-- fwtografia tou product -->\n                <a href=\"\">\n                    <img src=\"").concat(product.image, "\" alt=\"").concat(product.name, "\" class=\"product__image {\n                        height: 304px;\n                        width: 344px;\n                    }\">\n                </a>\n            </figure>\n        </article>"));
    }
  });
}; // section construct poy kalei tis synarthseis poy to xtizoun


exports.fillProducts = fillProducts;

var constructSection = function constructSection(productsArray, productsType, numberOfProducts) {
  fillHeadLine(productsArray, productsType);
  fillProducts(productsArray, numberOfProducts);
};

exports.constructSection = constructSection;
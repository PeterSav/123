"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _filterFunctions = require("./filterFunctions.js");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function fetchDevices() {
  return _fetchDevices.apply(this, arguments);
}

function _fetchDevices() {
  _fetchDevices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var URL, fetchResult, response, jsonData, resData, smartphones, tablets, accessories;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            URL = "https://cors-anywhere.herokuapp.com/https://gitlab.com/IsminiD/devices-json/raw/master/devices.json";
            fetchResult = fetch(URL);
            _context.next = 4;
            return fetchResult;

          case 4:
            response = _context.sent;
            _context.next = 7;
            return response.json();

          case 7:
            jsonData = _context.sent;
            resData = jsonData.devices;
            resData.sort(function (a, b) {
              return b.year - a.year;
            });
            smartphones = (0, _filterFunctions.getProduct)(resData);
            tablets = (0, _filterFunctions.getTablets)(resData);
            accessories = (0, _filterFunctions.getAccessories)(resData);
            (0, _filterFunctions.constructSection)(smartphones, "SmartPhones", 3);
            (0, _filterFunctions.constructSection)(tablets, "Tablets", 3);
            (0, _filterFunctions.constructSection)(accessories, "Accessories", 3);

          case 16:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _fetchDevices.apply(this, arguments);
}

var _default = fetchDevices;
exports.default = _default;
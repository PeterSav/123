"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// import {
//     getSmartphones,
//     getTablets,
//     getAccessories,
//     imagesSet,
//     pricesSet,
//     productsSet,
//     descriptionsSet,
//     asideButtonsShow
// } from "./functions"
var getSmartphones = function getSmartphones(myDevices) {
  return myDevices.filter(function (device) {
    return device.type === 'smartphone';
  });
};

var getTablets = function getTablets(myDevices) {
  return myDevices.filter(function (device) {
    return device.type === 'tablet';
  });
};

var getAccessories = function getAccessories(myDevices) {
  return myDevices.filter(function (device) {
    return device.type !== 'smartphone' && device.type !== 'tablet';
  });
};

var images = document.querySelectorAll('.image');

var imagesSet = function imagesSet(smartphones, tablets, accessories) {
  images.forEach(function (image, index) {
    if (index < 3) {
      image.setAttribute("src", "".concat(smartphones[index].image));
    } else if (index < 6) {
      image.setAttribute("src", "".concat(tablets[index - 3].image));
    } else {
      image.setAttribute("src", "".concat(accessories[index - 6].image));
    }
  });
};

var prices = document.querySelectorAll(".smartphonePrice");

var pricesSet = function pricesSet(smartphones, tablets, accessories) {
  prices.forEach(function (price, index) {
    if (index < 3) {
      price.innerHTML = "$".concat(smartphones[index].price);
    } else if (index < 6) {
      price.innerHTML = "$".concat(tablets[index - 3].price);
    } else {
      price.innerHTML = "$".concat(accessories[index - 6].price);
    }
  });
};

var products = document.querySelectorAll(".smartphonePhotoBoxTitle");

var productsSet = function productsSet(smartphones, tablets, accessories) {
  products.forEach(function (product, index) {
    if (index < 3) {
      product.innerHTML = "".concat(smartphones[index].name);
    } else if (index < 6) {
      product.innerHTML = "".concat(tablets[index - 3].name);
    } else {
      product.innerHTML = "".concat(accessories[index - 6].name);
    }
  });
};

var descriptions = document.querySelectorAll(".smartphonePhotoBoxSubtitle");

var descriptionsSet = function descriptionsSet(smartphones, tablets, accessories) {
  descriptions.forEach(function (description, index) {
    if (index < 3) {
      description.innerHTML = "".concat(smartphones[index].tech.display.Resolution);
    } else if (index < 6) {
      description.innerHTML = "".concat(tablets[index - 3].tech.display.Resolution);
    } else {
      description.innerHTML = "".concat(accessories[index - 6].tech.display.Resolution);
    }
  });
};

var asideButtons = document.querySelectorAll(".magenta");

var asideButtonsShow = function asideButtonsShow(smartphones, tablets, accessories) {
  asideButtons.forEach(function (button, index) {
    if (index < 3) {
      if (smartphones.length > 3) {
        button.style.visibility = "hidden";
      }
    } else if (index < 6) {
      if (tablets.length > 3) {
        button.style.visibility = "hidden";
      }
    } else {
      if (accessories.length > 3) {
        button.style.visibility = "hidden";
      }
    }
  });
};

function fetchDevices() {
  return _fetchDevices.apply(this, arguments);
}

function _fetchDevices() {
  _fetchDevices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var URL, fetchResult, response, jsonData, resData, smartphones, tablets, accessories;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            URL = "https://cors-anywhere.herokuapp.com/https://gitlab.com/IsminiD/devices-json/raw/master/devices.json";
            fetchResult = fetch(URL);
            _context.next = 4;
            return fetchResult;

          case 4:
            response = _context.sent;
            _context.next = 7;
            return response.json();

          case 7:
            jsonData = _context.sent;
            resData = jsonData.devices;
            resData.sort(function (a, b) {
              return b.year - a.year;
            });
            smartphones = getSmartphones(resData);
            tablets = getTablets(resData);
            accessories = getAccessories(resData);
            DOMselectors.smartNumb.innerHTML = "(".concat(smartphones.length, ")"); // refactor etsi

            document.getElementById('tabletNumb').innerHTML = "(".concat(tablets.length, ")");
            document.getElementById('accessoriesNumb').innerHTML = "(".concat(accessories.length, ")");
            imagesSet(smartphones, tablets, accessories);
            pricesSet(smartphones, tablets, accessories);
            productsSet(smartphones, tablets, accessories);
            descriptionsSet(smartphones, tablets, accessories);
            asideButtonsShow(smartphones, tablets, accessories);
            console.log(smartphones);

          case 22:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _fetchDevices.apply(this, arguments);
}

var DOMSelecors = {
  smartNumb: document.getElementById('smartNumb'),
  tabletNumb: document.getElementById('tabletNumb'),
  accessoriesNumb: document.getElementById('accessoriesNumb')
};
fetchDevices();